/*
 * Logging functions
 */
#ifndef NVT_LOG_H
#define NVT_LOG_H

#include <syslog.h>

#define _PRF_N_(no) __attribute__((__format__(__printf__, (no), (no) + 1)))
#define _PRF2_ _PRF_N_(2)

void            nvt_log_dest(int dest);
void            nvt_log_level(int quiet, int level);
_PRF2_ void     nvt_log(int prio, const char *fmt, ...);
void            nvt_log_buf(int prio, const char *txt,
                            const void *ptr, unsigned int len);

#define NVT_LOG_SYSLOG  0
#define NVT_LOG_STDOUT  1
#define NVT_LOG_STDERR  2

extern char     log_level;

#define DBG(...) \
    if (log_level > 0) nvt_log(LOG_DEBUG, __VA_ARGS__)
#define DBG2(...) \
    if (log_level > 1) nvt_log(LOG_DEBUG, __VA_ARGS__)
#define DBG2_BUF(txt, ptr, len) \
    if (log_level > 1) nvt_log_buf(LOG_DEBUG, txt, ptr, len)

#endif /* NVT_LOG_H */
